package com.alyubimkin.serverside.transfer;

import com.alyubimkin.gwtside.shared.DTO.EntityDTO;
import com.alyubimkin.gwtside.shared.DTO.PersonDTO;
import com.alyubimkin.serverside.entity.EntityI;
import com.alyubimkin.serverside.entity.Person;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class PersonTransfer implements  ModelMapperI{


    @Override
    public <T extends EntityDTO> T toDTO(EntityI entityI) {
        Person person = (Person) entityI;
        ModelMapper modelMapper = new ModelMapper();
        PersonDTO personDTO = modelMapper.map(person, PersonDTO.class);
        return (T) personDTO;
    }

    @Override
    public <T extends EntityI> T fromDTO(Class<? extends EntityDTO> myClass) {
        Class<? extends EntityDTO> personDTO = myClass;
        ModelMapper modelMapper = new ModelMapper();
        Person person = modelMapper.map(personDTO, Person.class);
        return (T) person;
    }

}
