package com.alyubimkin.serverside.transfer;

import com.alyubimkin.gwtside.shared.DTO.AddressDTO;
import com.alyubimkin.gwtside.shared.DTO.EntityDTO;
import com.alyubimkin.serverside.entity.Address;
import com.alyubimkin.serverside.entity.EntityI;
import org.modelmapper.ModelMapper;

public class AddressTransfer implements  ModelMapperI{


    @Override
    public <T extends EntityDTO> T toDTO(EntityI entityI) {
        Address address = (Address) entityI;
        ModelMapper modelMapper = new ModelMapper();
        AddressDTO addressDTO = modelMapper.map(address, AddressDTO.class);
        return (T) addressDTO;
    }

    @Override
    public <T extends EntityI> T fromDTO(Class<? extends EntityDTO> myClass) {
        Class<? extends EntityDTO> addressDTO = myClass;
        ModelMapper modelMapper = new ModelMapper();
        Address address = modelMapper.map(addressDTO, Address.class);
        return (T) address;
    }

}