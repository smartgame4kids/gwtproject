package com.alyubimkin.serverside.transfer;

import com.alyubimkin.gwtside.shared.DTO.ContractDTO;
import com.alyubimkin.gwtside.shared.DTO.EntityDTO;
import com.alyubimkin.serverside.entity.Contract;
import com.alyubimkin.serverside.entity.EntityI;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
public class ContractTransfer implements  ModelMapperI{


    @Override
    public <T extends EntityDTO> T toDTO(EntityI entityI) {
        Contract contract = (Contract) entityI;
        ModelMapper modelMapper = new ModelMapper();
        ContractDTO contractDTO = modelMapper.map(contract, ContractDTO.class);
        return (T) contractDTO;
    }

    @Override
    public <T extends EntityI> T fromDTO(Class<? extends EntityDTO> myClass) {
        Class<? extends EntityDTO> contractDTO = myClass;
        ModelMapper modelMapper = new ModelMapper();
        Contract contract = modelMapper.map(contractDTO, Contract.class);
        return (T) contract;
    }

}