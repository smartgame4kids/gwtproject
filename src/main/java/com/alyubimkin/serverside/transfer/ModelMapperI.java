package com.alyubimkin.serverside.transfer;

import com.alyubimkin.gwtside.shared.DTO.EntityDTO;
import com.alyubimkin.serverside.entity.EntityI;

public interface ModelMapperI {

    <T extends EntityDTO> T toDTO(EntityI entityI) ;

    <T extends EntityI> T fromDTO(Class<? extends EntityDTO> myClass);
}
