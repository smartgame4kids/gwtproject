package com.alyubimkin.serverside.service;

import com.alyubimkin.gwtside.shared.DTO.ContractDTO;

import java.util.List;

public interface ServiceContractI {

    List<ContractDTO> getAllContracts();

}
