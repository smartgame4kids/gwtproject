package com.alyubimkin.serverside.service;

import com.alyubimkin.serverside.DAO.PersonManagerI;
import com.alyubimkin.gwtside.shared.DTO.PersonDTO;
import com.alyubimkin.serverside.DAO.PersonManagerIImpl;
import com.alyubimkin.serverside.DAO.PersonRepositoryI;
import com.alyubimkin.serverside.entity.Person;
import com.alyubimkin.serverside.transfer.ModelMapperI;
import com.alyubimkin.serverside.transfer.PersonTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServicePersonImpl implements ServicePersonI {

    @Autowired
    PersonManagerIImpl personManagerImpl;

    @Autowired
    PersonRepositoryI personRepositoryI;

    @Autowired
    PersonTransfer personTransfer;


    @Transactional(readOnly = true)
    @Override
    public List<PersonDTO> getAllPerson() {
//        List<Person> personList = personManagerImpl.getPersons();
        List<Person> personList =  (List)  personRepositoryI.findAll();
        List<PersonDTO> personDTOList = personList.stream().map(person -> {
            PersonDTO personDTO =  personTransfer.toDTO(person);
            return personDTO;

        }).collect(Collectors.toList());
        return personDTOList;
    }
}
