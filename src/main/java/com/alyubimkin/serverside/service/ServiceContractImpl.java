package com.alyubimkin.serverside.service;


import com.alyubimkin.gwtside.shared.DTO.ContractDTO;
import com.alyubimkin.serverside.DAO.ContractRepositoryI;
import com.alyubimkin.serverside.entity.Contract;
import com.alyubimkin.serverside.transfer.ContractTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceContractImpl implements ServiceContractI {


    @Autowired
    ContractRepositoryI contractRepositoryI;

    @Autowired
    ContractTransfer contractTransfer;


    @Transactional(readOnly = true)
    @Override
    public List<ContractDTO> getAllContracts() {
        List<Contract> contractList =  (List)  contractRepositoryI.findAll();
        List<ContractDTO> contractDTOList = contractList.stream().map(person -> {
            ContractDTO contractDTO =  contractTransfer.toDTO(person);
            return contractDTO;

        }).collect(Collectors.toList());
        return contractDTOList;
    }
}
