package com.alyubimkin.serverside.service;

import com.alyubimkin.gwtside.shared.DTO.PersonDTO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ServicePersonI {

    List<PersonDTO> getAllPerson();
}
