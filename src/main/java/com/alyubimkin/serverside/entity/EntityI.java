package com.alyubimkin.serverside.entity;

import java.io.Serializable;

public interface EntityI extends Serializable {

    Integer getId();
}
