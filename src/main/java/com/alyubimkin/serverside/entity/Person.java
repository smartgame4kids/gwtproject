package com.alyubimkin.serverside.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "person")

public class Person implements EntityI  {


    @Id
    @GeneratedValue
    @Column(name = "person_id")
    private Integer id;

    @NotNull
    @Column(name = "family")
    private String family;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "middlename")
    private String middleName;

    @NotNull
    @Column(name = "birthday")
    private Date birthDay;

    @NotNull
    @Column(name = "pass_serial")
    private  Integer passSerial;

    @NotNull
    @Column(name = "pass_number")
    private  Integer passNumber;

    @NotNull
    @OneToMany
    @JoinColumn(name = "address_id")
    private Set<Address> addresses = new HashSet<Address>();


    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public Integer getPassSerial() {
        return passSerial;
    }

    public void setPassSerial(Integer passSerial) {
        this.passSerial = passSerial;
    }

    public Integer getPassNumber() {
        return passNumber;
    }

    public void setPassNumber(Integer passNumber) {
        this.passNumber = passNumber;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
