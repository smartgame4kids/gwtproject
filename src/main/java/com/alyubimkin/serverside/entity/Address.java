package com.alyubimkin.serverside.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "address")
public class Address implements EntityI {

    @Id
    @GeneratedValue
    @Column(name = "address_id")
    private Integer id;

    @NotNull
    @Column(name = "country")
    private String country;

    @NotNull
    @Column(name = "homeIndex")
    private String homeIndex;

    @NotNull
    @Column(name = "region")
    private String region;

    @NotNull
    @Column(name = "district")
    private String district;

    @NotNull
    @Column(name = "city")
    private String city;

    @NotNull
    @Column(name = "street")
    private String street;

    @NotNull
    @Column(name = "house")
    private Integer house;

    @NotNull
    @Column(name = "build")
    private String build;

    @NotNull
    @Column(name = "apartment")
    private String apartment;

    @OneToOne
    @JoinColumn(name = "person_id")
    private Person person;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getHomeIndex() {
        return homeIndex;
    }

    public void setHomeIndex(String homeIndex) {
        this.homeIndex = homeIndex;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }

    public String getBuild() {
        return build;
    }

    public void setBuild(String build) {
        this.build = build;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
