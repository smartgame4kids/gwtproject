package com.alyubimkin.serverside.DAO;

import com.alyubimkin.serverside.entity.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class PersonManagerIImpl implements PersonManagerI {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Person> getPersons() {
        return em.createQuery("SELECT p FROM Person p").getResultList();
    }
}
