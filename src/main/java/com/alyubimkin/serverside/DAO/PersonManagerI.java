package com.alyubimkin.serverside.DAO;

import com.alyubimkin.serverside.entity.Person;
import org.springframework.stereotype.Component;

import java.util.List;

public interface PersonManagerI {
   List<Person> getPersons();
}
