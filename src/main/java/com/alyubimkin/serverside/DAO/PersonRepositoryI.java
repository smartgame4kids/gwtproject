package com.alyubimkin.serverside.DAO;

import com.alyubimkin.serverside.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

public interface PersonRepositoryI extends CrudRepository<Person, Long> {
}