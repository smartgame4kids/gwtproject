package com.alyubimkin.serverside.DAO;

import com.alyubimkin.serverside.entity.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepositoryI  extends CrudRepository<Contract, Long> {
}
