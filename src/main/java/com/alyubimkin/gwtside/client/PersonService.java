package com.alyubimkin.gwtside.client;

import com.alyubimkin.gwtside.shared.DTO.PersonDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;



@RemoteServiceRelativePath("springGwtServices/personService")
public interface PersonService extends RemoteService {
  List<PersonDTO> getPersons();
}
