package com.alyubimkin.gwtside.client;


import com.alyubimkin.gwtside.shared.DTO.ContractDTO;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

@RemoteServiceRelativePath("springGwtServices/contractService")
public interface ContractService extends RemoteService {
    List<ContractDTO> getContracts();
}
