package com.alyubimkin.gwtside.client;

import com.alyubimkin.gwtside.shared.DTO.PersonDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.DatePicker;

import java.awt.*;
import java.util.Date;
import java.util.List;

public class ContractDialog extends DialogBox {
//    @UiTemplate("ContractDialog.ui.xml")
//    interface ContractDialogUiBinder extends UiBinder<Widget, ContractDialog> {}
//    private static final ContractDialog.ContractDialogUiBinder uiBinder = GWT.create(MyButton.MyButtonUiBinder.class);

    private final PersonServiceAsync contractService = GWT.create(PersonService.class);

    public ContractDialog() {
        super(false, true);
        super.setHeight("800");
        super.setWidth("800");
        center();
        setText("Создать договор");
        Button createButton = new Button("Создать договор");
        createButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

            }
        });

        Button closeButton = new Button("Отмена");
        closeButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                hide();
            }
        });
        Label labelSumm = new Label("Страховая сумма");
        TextBox summ = new TextBox();

        VerticalPanel mainH = new VerticalPanel();
        mainH.setWidth("790");
        mainH.setHeight("790");


        HorizontalPanel main1 = new HorizontalPanel();
        HorizontalPanel main2 = new HorizontalPanel();
        main2.add(labelSumm);
        main2.add(summ);


        HorizontalPanel main4 = new HorizontalPanel();
        Label labelPeriod = new Label("Срок действия с ");
        DatePicker datePicker = new DatePicker();

        TextBox textBox = new TextBox();
        Image icon = new Image("calendar.png");
        datePicker.addValueChangeHandler(new ValueChangeHandler<Date>() {

            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                Date date = event.getValue();
                String dateStr = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM).format(date);
                textBox.setText(dateStr);
                datePicker.setVisible(false);
            }
        });
        datePicker.setVisible(false);
        icon.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                datePicker.setVisible(true);
            }
        });
        main4.add(labelPeriod);
        main4.add(icon);
        main4.add(textBox);
        main4.add(datePicker);

        main1.add(main2);
        HorizontalPanel hdelim = new HorizontalPanel();
        hdelim.setWidth("30");
        main1.add(hdelim);
        main1.add(main4);


        HorizontalPanel main3 = new HorizontalPanel();
        main3.add(createButton);
        main3.add(closeButton);
        mainH.setWidth("100%");
        contractService.getPersons(new AsyncCallback<List<PersonDTO>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(List<PersonDTO> result) {
                summ.setText(result.get(0).getFamily());
            }
        });
        mainH.add(main1);
        mainH.add(main3);
        setWidget(mainH);
    }

}
