package com.alyubimkin.gwtside.client;

import com.alyubimkin.gwtside.shared.DTO.ContractDTO;
import com.alyubimkin.gwtside.shared.DTO.ContractDTO;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.*;

import java.util.ArrayList;
import java.util.List;

public class ContractGrid extends Composite {

    private static PersonGridUiBinder uiBinder = GWT.create(PersonGridUiBinder.class);

    private final ContractServiceAsync contractService = GWT.create(ContractService.class);

    /*
     * @UiTemplate is not mandatory but allows multiple XML templates
     * to be used for the same widget.
     * Default file loaded will be <class-name>.ui.xml
     */
    @UiTemplate("ContractGrid.ui.xml")
    interface PersonGridUiBinder extends UiBinder<Widget, ContractGrid> {
    }

    @UiField
    DataGrid<ContractDTO> dataGrid;

    @UiField
    Button createContract;

    @UiField
    Button openContract;

    @UiField
    SimplePager pager;

    private ContractDTO selected;



    public ContractGrid() {
        initWidget(uiBinder.createAndBindUi(this));
        contractService.getContracts(new AsyncCallback<List<ContractDTO>>() {
            @Override
            public void onFailure(Throwable throwable) {
                createDataGrid(new ArrayList<ContractDTO>());
            }

            @Override
            public void onSuccess(List<ContractDTO> contractDTOS) {
                createDataGrid(contractDTOS);
            }
        });
    }

    private void createDataGrid(List<ContractDTO> contractDTOS){
        dataGrid.setTitle("Договора");
        TextColumn<ContractDTO> contractNumber = new TextColumn<ContractDTO>() {
            @Override
            public String getValue(ContractDTO contractDTO) {
                return contractDTO.getContractNumber();
            }
        };
        dataGrid.addColumn(contractNumber, "Серия-Номер");

        TextColumn<ContractDTO> startDate = new TextColumn<ContractDTO>() {
            @Override
            public String getValue(ContractDTO contractDTO) {
                return contractDTO.getStartDate().toString();
            }
        };
        dataGrid.addColumn(startDate, "Дата заключения");


        TextColumn<ContractDTO> fio = new TextColumn<ContractDTO>() {
            @Override
            public String getValue(ContractDTO contractDTO) {
                return contractDTO.getPerson().getFamily() + " " + contractDTO.getPerson().getName() + " " + contractDTO.getPerson().getMiddleName();
            }
        };
        dataGrid.addColumn(fio, "Страхователь");

        TextColumn<ContractDTO> period = new TextColumn<ContractDTO>() {
            @Override
            public String getValue(ContractDTO contractDTO) {
                return contractDTO.getStartDate().toString() + "-" + contractDTO.getEndDate().toString();
            }
        };
        dataGrid.addColumn(period, "Срок действия");

        final SingleSelectionModel<ContractDTO> selectionModel =
                new SingleSelectionModel<ContractDTO>();

        dataGrid.setSelectionModel(selectionModel);
        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent selectionChangeEvent) {
                 selected = selectionModel.getSelectedObject();
            }
        });
        dataGrid.setRowCount(contractDTOS.size(), true);
        dataGrid.setWidth("100%");

        dataGrid.setRowData(0, contractDTOS);
        // Create a Pager to control the table.
//        SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
//        pager = new SimplePager(SimplePager.TextLocation.CENTER, pagerResources, false, 0, true);
        pager.setDisplay(dataGrid);

        createContractButton();
        createOpenButton();
    }

     private void createContractButton(){
        createContract.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                ContractDialog contractDialog = new ContractDialog();
                contractDialog.show();
//                Window.alert(selected.getContractNumber());
            }
        });

     }

    private void createOpenButton(){
        openContract.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {

            }
        });
    }

}
