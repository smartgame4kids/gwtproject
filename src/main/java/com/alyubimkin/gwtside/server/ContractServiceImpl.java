package com.alyubimkin.gwtside.server;

import com.alyubimkin.gwtside.client.ContractService;
import com.alyubimkin.gwtside.shared.DTO.ContractDTO;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ContextLoader;

import java.util.List;

@Service
public class ContractServiceImpl extends RemoteServiceServlet implements
        ContractService {


    @Override
    public List<ContractDTO> getContracts() {
        ContractServiceImpl contractServiceImpl = (ContractServiceImpl) ContextLoader.getCurrentWebApplicationContext().getBean("serviceContractImpl");
        return contractServiceImpl.getContracts();
    }

}
