package com.alyubimkin.gwtside.server;

import com.alyubimkin.gwtside.client.PersonService;
import com.alyubimkin.gwtside.shared.DTO.PersonDTO;
import com.alyubimkin.serverside.service.ServicePersonImpl;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ContextLoader;

import java.util.List;

/**
 * The server side implementation of the RPC service.
 */
@Service
public class PersonServiceImpl extends RemoteServiceServlet implements
        PersonService {


  @Override
  public List<PersonDTO> getPersons() {
    ServicePersonImpl servicePersonImpl = (ServicePersonImpl) ContextLoader.getCurrentWebApplicationContext().getBean("servicePersonImpl");
    return servicePersonImpl.getAllPerson();
  }



  /**
   * Escape an html string. Escaping data received from the client helps to
   * prevent cross-site script vulnerabilities.
   *
   * @param html the html string to escape
   * @return the escaped string
   */
  private String escapeHtml(String html) {
    if (html == null) {
      return null;
    }
    return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(
        ">", "&gt;");
  }
}
