package com.alyubimkin.gwtside.shared.DTO;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

public class PersonDTO implements EntityDTO{

    private Integer id;

    private String family;

    private String name;


    private String middleName;


    private Date birthDay;


    private  Integer passSerial;

    private  Integer passNumber;

    private Set<AddressDTO> addresses = new HashSet<AddressDTO>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public Integer getPassSerial() {
        return passSerial;
    }

    public void setPassSerial(Integer passSerial) {
        this.passSerial = passSerial;
    }

    public Integer getPassNumber() {
        return passNumber;
    }

    public void setPassNumber(Integer passNumber) {
        this.passNumber = passNumber;
    }

    public Set<AddressDTO> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AddressDTO> addresses) {
        this.addresses = addresses;
    }
}
